package com.avappinc.sounds;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ChooseActivity extends AppCompatActivity {

    public static final String REGIME = "REGIME";
    public static final String LIST = "LIST";

    private ArrayList<String> userList = new ArrayList<>();
    private ImageView goNextImageView;
    private TextView textView;
    private CheckBox checkBoxAll;
    private Order order;

    public static int getResourceId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        textView = findViewById(R.id.textView);
        textView.setText(R.string.chooseTextView);

        textView.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.regimeTitle)
                    .setItems(R.array.order, (dialog, which) -> {
                        order = Order.getOrderById(which);
                        textView.setText(order.getText());
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        goNextImageView = findViewById(R.id.goView);
        goNextImageView.setOnClickListener(v -> {
            if (order == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.regimeTitle)
                        .setItems(R.array.order, (dialog, which) -> {
                            order = Order.getOrderById(which);
                            textView.setText(order.getText());
                            openCategoryActivity();
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                openCategoryActivity();
            }
        });

        checkBoxAll = findViewById(R.id.all);
        checkBoxAll.setOnClickListener(v -> {
            userList.clear();
            String[] str = getResources().getStringArray(R.array.number);

            for (int i = 1; i <= 33; i++) {
                int tempImageViewId = getResourceId("imageView" + i, R.id.class);
                int tempLetterViewId = getResourceId("letterView" + i, R.id.class);
                if (checkBoxAll.isChecked()) {
                    ImageView temp = findViewById(tempImageViewId);
                    findViewById(tempLetterViewId).setBackgroundColor(getResources().getColor(R.color.choose));
                    temp.setVisibility(View.VISIBLE);

                    int drawableId = getResources().getIdentifier(str[i - 1], "drawable", "com.avappinc.sounds");
                    temp.setImageResource(drawableId);
                    if (!userList.contains(findViewById(tempLetterViewId).getResources()
                            .getResourceEntryName(findViewById(tempLetterViewId).getId()))) {
                        userList.add(findViewById(tempLetterViewId).getResources()
                                .getResourceEntryName(findViewById(tempLetterViewId).getId()));
                    }

                } else {
                    findViewById(tempLetterViewId).setBackgroundColor(getResources().getColor(R.color.Polina1));
                    findViewById(tempImageViewId).setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    public void letterClick(View v) {
        ImageView view = findViewById(v.getId());
        String[] str = getResources().getStringArray(R.array.number);
        if (view.getBackground() == null) {
            view.setBackgroundColor(getResources().getColor(R.color.choose));

            if (!userList.contains(view.getResources().getResourceEntryName(view.getId()))) {
                userList.add(view.getResources().getResourceEntryName(view.getId()));
            }

            if (userList.size() == 33) {
                checkBoxAll = findViewById(R.id.all);
                checkBoxAll.setChecked(true);
            }

            int i = 0;
            while (i < userList.size()) {
                String num;
                //String viewName = getResources().getResourceName(view.getId());

                if (userList.get(i).charAt(userList.get(i).length() - 2) == '1' ||
                        userList.get(i).charAt(userList.get(i).length() - 2) == '2' ||
                        userList.get(i).charAt(userList.get(i).length() - 2) == '3') {
                    num = Character.toString(userList.get(i).charAt(userList.get(i).length() - 2)) +
                            userList.get(i).charAt(userList.get(i).length() - 1);
                } else {
                    num = Character.toString(userList.get(i).charAt(userList.get(i).length() - 1));
                }

                ImageView temp = findViewById(getResourceId("imageView" + num, R.id.class));
                int drawableId = getResources().getIdentifier(str[i], "drawable", "com.avappinc.sounds");
                temp.setImageResource(drawableId);
                temp.setVisibility(View.VISIBLE);
                i++;
            }
        } else {
            while (userList.contains(view.getResources().getResourceEntryName(view.getId()))) {
                userList.remove(view.getResources().getResourceEntryName(view.getId()));
            }
            view.setBackground(null);
            checkBoxAll = findViewById(R.id.all);
            checkBoxAll.setChecked(false);
            for (int j = 1; j <= 33; j++) {
                int tempImageViewId = getResourceId("imageView" + j, R.id.class);
                ImageView temp = findViewById(tempImageViewId);
                temp.setVisibility(View.INVISIBLE);
            }
            int i = 0;
            while (i < userList.size()) {
                String num;
                //String viewName = getResources().getResourceName(view.getId());

                if (userList.get(i).charAt(userList.get(i).length() - 2) == '1' ||
                        userList.get(i).charAt(userList.get(i).length() - 2) == '2' ||
                        userList.get(i).charAt(userList.get(i).length() - 2) == '3') {
                    num = Character.toString(userList.get(i).charAt(userList.get(i).length() - 2)) +
                            userList.get(i).charAt(userList.get(i).length() - 1);
                } else {
                    num = Character.toString(userList.get(i).charAt(userList.get(i).length() - 1));
                }

                ImageView temp = findViewById(getResourceId("imageView" + num, R.id.class));
                int drawableId = getResources().getIdentifier(str[i], "drawable", "com.avappinc.sounds");
                temp.setImageResource(drawableId);
                temp.setVisibility(View.VISIBLE);
                i++;
            }
        }
    }

    public void openCategoryActivity() {
        Intent intent = new Intent(this, CategoryActivity.class);
        if (!userList.isEmpty()) {
            intent.putExtra(LIST, userList);
            intent.putExtra(REGIME, order);
            startActivity(intent);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(R.string.toolessDialogTitle);
            builder.setMessage(R.string.toolessLetters);
            builder.setPositiveButton(R.string.ok, (dialog, which) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

}
