package com.avappinc.sounds;

public enum Order {
    ABC(0, "Алфавит"),
    ONTO(1, "Онтогенез"),
    USER(2, "Пользовательский");

    private final int id;
    private final String text;

    Order(int regime, String text) {
        this.id = regime;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static Order getOrderById(int id) {
        for (Order order:  Order.values()){
            if (order.id == id) {
                return order;
            }
        }
        return ABC;
    }
}
