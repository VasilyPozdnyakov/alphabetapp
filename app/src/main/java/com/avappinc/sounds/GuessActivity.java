package com.avappinc.sounds;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.avappinc.sounds.InitialActivity.CURRENT_LEVEL;
import static com.avappinc.sounds.InitialActivity.activityElementsLists;
import static com.avappinc.sounds.Music.musicPool;

public class GuessActivity extends AppCompatActivity {

    private static final int CURRENT_LEVEL_HEIGHT = 60;
    private static final int DEFAULT_LEVEL_HEIGHT = 40;
    private static final int DELAY_IN_MILLIS = 1000;

    private int correctIndex = 0;
    private int leftTopIndex = 0, leftBottomIndex = 0, rightTopIndex = 0, rightBottomIndex = 0;
    private ImageView leftTopImage, rightTopImage, leftBottomImage, rightBottomImage, correctImage, correctLetter;
    private ImageView musicView, updateView;
    private ImageView easyLevelImageView, mediumLevelImageView, hardLevelImageView;
    private LinearLayout bottomLayout;
    private Level currentLevel;
    private float scale;
    private Boolean isMusicPlayed = false, isMusicCanPlay = true, winPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess);

        scale = getBaseContext().getResources().getDisplayMetrics().density;
        initView();

        ElementsLists elementsList = activityElementsLists;
        final List<ActivityElement> activityElementList = getActivityElementList(elementsList);

        initCurrentLevel();
        int guessListSize = elementsList.getGuessList().size();
        refreshElementsView(activityElementList, guessListSize);
        refreshActivityView();

        musicView.setOnClickListener(
                v -> {
                    if (isMusicCanPlay) {
                        musicPool = MediaPlayer.create(this, activityElementList.get(correctIndex).getSound1());
                        musicPool.setOnPreparedListener(mp -> musicPool.start());
                        musicPool.setOnCompletionListener(mp -> musicPool.release());
                        isMusicPlayed = true;
                    }

                });

        updateView.setOnClickListener(
                v -> {
                    if (!winPlaying) {
                        refreshElementsView(activityElementList, guessListSize);
                        Music.INSTANCE.pauseMusic();
                        isMusicPlayed = false;
                    }
                }
        );

        setOnClickListenerToLevelView(activityElementList, easyLevelImageView, Level.EASY, guessListSize);
        setOnClickListenerToLevelView(activityElementList, mediumLevelImageView, Level.MEDIUM, guessListSize);
        setOnClickListenerToLevelView(activityElementList, hardLevelImageView, Level.HARD, guessListSize);
        setOnClickListenerToElementView(activityElementList, guessListSize);
    }


    private void setOnClickListenerToLevelView(final List<ActivityElement> activityElementList, ImageView easyLevelImageView, final Level level, int guessListSize) {
        easyLevelImageView.setOnClickListener(v -> {
            if (isMusicCanPlay && currentLevel != level) {
                currentLevel = level;
                refreshActivityView();
                refreshElementsView(activityElementList, guessListSize);
                Music.INSTANCE.pauseMusic();
                isMusicPlayed = false;
            }
        });
    }

    private void setOnClickListenerToElementView(final List<ActivityElement> activityElementList, int guessListSize) {
        Handler handler = new Handler();
        leftTopImage.setOnClickListener(
                v -> {
                    if (isMusicPlayed) {
                        if (correctIndex == leftTopIndex) {
                            showCorrectAnswer(handler, activityElementList, guessListSize);
                        } else {
                            incorrectAnswer(leftTopImage);
                        }
                    }
                }
        );
        rightTopImage.setOnClickListener(
                v -> {
                    if (isMusicPlayed) {
                        if (correctIndex == rightTopIndex) {
                            showCorrectAnswer(handler, activityElementList, guessListSize);
                        } else {
                            incorrectAnswer(rightTopImage);
                        }
                    }
                }
        );
        rightBottomImage.setOnClickListener(
                v -> {
                    if (isMusicPlayed) {
                        if (correctIndex == rightBottomIndex) {
                            showCorrectAnswer(handler, activityElementList, guessListSize);
                        } else {
                            incorrectAnswer(rightBottomImage);
                        }
                    }
                }
        );
        leftBottomImage.setOnClickListener(
                v -> {
                    if (isMusicPlayed) {
                        if (correctIndex == leftBottomIndex) {
                            showCorrectAnswer(handler, activityElementList, guessListSize);
                        } else {
                            incorrectAnswer(leftBottomImage);
                        }
                    }
                }
        );
    }

    private void incorrectAnswer(ImageView imageView) {
        imageView.setVisibility(View.INVISIBLE);
    }

    private void showCorrectAnswer(Handler handler, List<ActivityElement> activityElementList, int guessListSize) {
        Music.INSTANCE.pauseMusic();
        isMusicCanPlay = false;
        musicPool = MediaPlayer.create(this, R.raw.win);
        musicPool.setOnPreparedListener(mp -> musicPool.start());
        musicPool.setOnCompletionListener(mp -> musicPool.release());
        winPlaying = true;
        correctLetter.setImageDrawable(getResources().getDrawable(activityElementList.get(correctIndex).getImage1()));
        correctImage.setImageDrawable(getResources().getDrawable(activityElementList.get(correctIndex).getImage2()));
        leftBottomImage.setVisibility(View.INVISIBLE);
        leftTopImage.setVisibility(View.INVISIBLE);
        rightTopImage.setVisibility(View.INVISIBLE);
        rightBottomImage.setVisibility(View.INVISIBLE);
        isMusicPlayed = false;
        correctImage.setVisibility(View.VISIBLE);
        correctLetter.setVisibility(View.VISIBLE);
        handler.postDelayed(() -> refreshElementsView(activityElementList, guessListSize), DELAY_IN_MILLIS);
    }

    private void refreshElementsView(List<ActivityElement> activityElementList, int guessListSize) {
        winPlaying = false;
        correctIndex = resetIndexes(guessListSize, activityElementList.size());
        leftTopImage.setImageDrawable(getResources().getDrawable(activityElementList.get(leftTopIndex).getImage1()));
        rightTopImage.setImageDrawable(getResources().getDrawable(activityElementList.get(rightTopIndex).getImage1()));
        leftBottomImage.setImageDrawable(getResources().getDrawable(activityElementList.get(leftBottomIndex).getImage1()));
        rightBottomImage.setImageDrawable(getResources().getDrawable(activityElementList.get(rightBottomIndex).getImage1()));
        refreshActivityView();
        isMusicCanPlay = true;
    }

    private void initCurrentLevel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            currentLevel = Level.valueOf(extras.getString(CURRENT_LEVEL));
        } else {
            currentLevel = Level.EASY;
        }
    }

    private void refreshActivityView() {
        switch (currentLevel) {
            case HARD:
                refreshLevelImage(easyLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.one);
                refreshLevelImage(mediumLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.two);
                refreshLevelImage(hardLevelImageView, CURRENT_LEVEL_HEIGHT, R.drawable.three_red);
                leftBottomImage.setVisibility(View.VISIBLE);
                leftTopImage.setVisibility(View.VISIBLE);
                rightTopImage.setVisibility(View.VISIBLE);
                rightBottomImage.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                correctImage.setVisibility(View.INVISIBLE);
                correctLetter.setVisibility(View.INVISIBLE);
                break;
            case MEDIUM:
                refreshLevelImage(easyLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.one);
                refreshLevelImage(mediumLevelImageView, CURRENT_LEVEL_HEIGHT, R.drawable.two_red);
                refreshLevelImage(hardLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.three);
                leftBottomImage.setVisibility(View.VISIBLE);
                rightBottomImage.setVisibility(View.GONE);
                leftTopImage.setVisibility(View.VISIBLE);
                rightTopImage.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                correctImage.setVisibility(View.INVISIBLE);
                correctLetter.setVisibility(View.INVISIBLE);
                break;
            case EASY:
            default:
                refreshLevelImage(easyLevelImageView, CURRENT_LEVEL_HEIGHT, R.drawable.one_red);
                refreshLevelImage(mediumLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.two);
                refreshLevelImage(hardLevelImageView, DEFAULT_LEVEL_HEIGHT, R.drawable.three);
                leftBottomImage.setVisibility(View.GONE);
                rightBottomImage.setVisibility(View.GONE);
                leftTopImage.setVisibility(View.VISIBLE);
                rightTopImage.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.GONE);
                correctImage.setVisibility(View.INVISIBLE);
                correctLetter.setVisibility(View.INVISIBLE);
        }
    }

    private void refreshLevelImage(ImageView imageView, int heightInDp, int imageResource) {
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        layoutParams.height = covertDpToPixels(heightInDp);
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(imageResource);
        correctImage.setVisibility(View.INVISIBLE);
        correctLetter.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        leftTopImage = findViewById(R.id.leftTopView);
        rightTopImage = findViewById(R.id.rightTopView);
        leftBottomImage = findViewById(R.id.leftBottomView);
        rightBottomImage = findViewById(R.id.rightBottomView);
        musicView = findViewById(R.id.musicView);
        updateView = findViewById(R.id.updateView);
        bottomLayout = findViewById(R.id.bottomLayout);
        easyLevelImageView = findViewById(R.id.easyLevelImageView);
        mediumLevelImageView = findViewById(R.id.mediumLevelImageView);
        hardLevelImageView = findViewById(R.id.hardLevelImageView);
        correctImage = findViewById(R.id.correctView);
        correctLetter = findViewById(R.id.correctLetterView);
    }

    @Override
    public void finish() {
        super.finish();
        activityElementsLists.saveGuessList(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMusic();
    }

    private int covertDpToPixels(int dp) {
        return (int) (dp * scale + 0.5f);
    }

    private List<ActivityElement> getActivityElementList(ElementsLists elementsList) {
        ArrayList<ActivityElement> arrayList = new ArrayList<>(elementsList.getGuessList());
        if (arrayList.size() < 4) {
            for (ActivityElement element : elementsList.getRandomList()) {
                if (!arrayList.contains(element)) {
                    arrayList.add(element);
                }
            }
        }
        return arrayList;
    }

    private int resetIndexes(int guessListSize, int listSize) {
        if (guessListSize < 4) {
            return resetIndexesGuessListSizeLess4(guessListSize, listSize);
        }
        Random random = new Random();
        setLeftTopIndex(listSize, random, 0);
        setRightTopIndex(listSize, random, 0);
        setLeftBottomIndex(listSize, random, 0);
        setRightBottomIndex(listSize, random, 0);
        return getRandomIndex();
    }

    private void setLeftTopIndex(int listSize, Random random, int seek) {
        leftTopIndex = random.nextInt(listSize - seek) + seek;
    }

    private void setRightTopIndex(int listSize, Random random, int seek) {
        do {
            rightTopIndex = random.nextInt(listSize - seek) + seek;
        } while (leftTopIndex == rightTopIndex);
    }

    private void setLeftBottomIndex(int listSize, Random random, int seek) {
        do {
            leftBottomIndex = random.nextInt(listSize - seek) + seek;
        } while (leftBottomIndex == leftTopIndex || leftBottomIndex == rightTopIndex);
    }

    private void setRightBottomIndex(int listSize, Random random, int seek) {
        do {
            rightBottomIndex = random.nextInt(listSize - seek) + seek;
        } while (rightBottomIndex == leftTopIndex || rightBottomIndex == leftBottomIndex || rightBottomIndex == rightTopIndex);
    }

    private int getRandomIndex() {
        Random random = new Random();
        switch (currentLevel) {
            case HARD:
                return random.nextBoolean() ? random.nextBoolean() ? leftTopIndex : rightTopIndex : random.nextBoolean() ? leftBottomIndex : rightBottomIndex;
            case MEDIUM:
                return random.nextBoolean() ? random.nextBoolean() ? leftTopIndex : rightTopIndex : leftBottomIndex;
            case EASY:
            default:
                return random.nextBoolean() ? leftTopIndex : rightTopIndex;
        }
    }

    private int resetIndexesGuessListSizeLess4(int guessListSize, int listSize) {
        Random random = new Random();
        switch (currentLevel) {
            case HARD:
                if (random.nextBoolean()) {
                    if (random.nextBoolean()) {
                        setLeftTopIndex(guessListSize, random, 0);
                        setRightTopIndex(listSize, random, guessListSize);
                        setLeftBottomIndex(listSize, random, guessListSize);
                        setRightBottomIndex(listSize, random, guessListSize);
                        return leftTopIndex;
                    } else {
                        rightTopIndex = random.nextInt(guessListSize);
                        do {
                            leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                        } while (leftTopIndex == rightTopIndex);
                        setLeftBottomIndex(listSize, random, guessListSize);
                        setRightBottomIndex(listSize, random, guessListSize);
                        return rightTopIndex;
                    }
                } else if (random.nextBoolean()) {
                    leftBottomIndex = random.nextInt(guessListSize);
                    do {
                        leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (leftTopIndex == rightTopIndex);
                    do {
                        rightTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (rightTopIndex == leftTopIndex || leftBottomIndex == rightTopIndex);
                    setRightBottomIndex(listSize, random, guessListSize);
                    return leftBottomIndex;
                } else {
                    rightBottomIndex = random.nextInt(guessListSize);
                    do {
                        leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (leftTopIndex == rightTopIndex);
                    do {
                        rightTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (rightTopIndex == leftTopIndex || leftBottomIndex == rightTopIndex);
                    do {
                        leftBottomIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (leftBottomIndex == leftTopIndex || rightBottomIndex == leftBottomIndex || leftBottomIndex == rightTopIndex);
                    return rightBottomIndex;
                }
            case MEDIUM:
                if (random.nextBoolean()) {
                    if (random.nextBoolean()) {
                        setLeftTopIndex(guessListSize, random, 0);
                        setRightTopIndex(listSize, random, guessListSize);
                        setLeftBottomIndex(listSize, random, guessListSize);
                        return leftTopIndex;
                    } else {
                        rightTopIndex = random.nextInt(guessListSize);
                        do {
                            leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                        } while (leftTopIndex == rightTopIndex);
                        setLeftBottomIndex(listSize, random, guessListSize);
                        return rightTopIndex;
                    }
                } else {
                    leftBottomIndex = random.nextInt(guessListSize);
                    do {
                        leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (leftTopIndex == rightTopIndex);
                    do {
                        rightTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (rightTopIndex == leftTopIndex || leftBottomIndex == rightTopIndex);
                    return leftBottomIndex;
                }
            case EASY:
            default:
                if (random.nextBoolean()) {
                    setLeftTopIndex(guessListSize, random, 0);
                    setRightTopIndex(listSize, random, guessListSize);
                    return leftTopIndex;
                } else {
                    rightTopIndex = random.nextInt(guessListSize);
                    do {
                        leftTopIndex = random.nextInt(listSize - guessListSize) + guessListSize;
                    } while (leftTopIndex == rightTopIndex);
                    return rightTopIndex;
                }
        }
    }
}
