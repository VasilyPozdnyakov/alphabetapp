package com.avappinc.sounds;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import static com.avappinc.sounds.Music.musicPool;
import static com.avappinc.sounds.SettingsActivity.STUDY_SWITCH;

public class InitialActivity extends AppCompatActivity {

    public static final String CURRENT_LEVEL = "CURRENT_LEVEL";

    public static ElementsLists activityElementsLists;

    private ImageView category, game, settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        category = findViewById(R.id.categoryView);
//        abc = findViewById(R.id.abcView);
//        user = findViewById(R.id.userView);
        game = findViewById(R.id.gameView);
        settings = findViewById(R.id.settingsView);

        initMusicPool();
        activityElementsLists = createAndFillElementsLists();

//        Toast toast = Toast.makeText(getApplicationContext(), "Current lang: "+lang, Toast.LENGTH_SHORT);
//        toast.show();

        category.setOnClickListener(v -> openChooseActivity());

        game.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    if (!activityElementsLists.getGuessList().isEmpty()) {
                        openGuessActivity();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setCancelable(true);
                        builder.setTitle(R.string.toolessDialogTitle);
                        builder.setMessage(R.string.toolessElements);
                        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
        );
        settings.setOnClickListener(v -> openSettingsActivity());
    }

    @Override
    public void finish() {
        super.finish();
        musicPool.release();
        activityElementsLists.saveGuessList(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMainMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.INSTANCE.playMusic(this);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getBoolean(STUDY_SWITCH, false)) {
            activityElementsLists.setGuessList(InitialActivity.activityElementsLists.getRandomList());
        } else {
            activityElementsLists.loadGuessActivity(this);
        }
    }

    private void initMusicPool() {
        Music.INSTANCE.initMusic(this);
        Music.INSTANCE.playMusic(this);
        // musicPool.setOnLoadCompleteListener((soundPool, sampleId, status) -> {
//            if (sampleId >= activityElementsLists.getRandomList().size()) {
//                Toast toast = Toast.makeText(getApplicationContext(), "Загрузка музыки окончена..", Toast.LENGTH_SHORT);
//                toast.show();
//            }
        //});
    }

    public void openChooseActivity() {
        Intent intent = new Intent(this, ChooseActivity.class);
        //intent.putExtra("REGIME",regime);
        startActivity(intent);
    }

    public void openGuessActivity() {
        Intent intent = new Intent(this, GuessActivity.class);
        intent.putExtra(CURRENT_LEVEL, Level.EASY.name());
        startActivity(intent);
    }

    public void openSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }


    private ElementsLists createAndFillElementsLists() {
        final ElementsLists elementsLists = new ElementsLists();
        elementsLists.setLettersList(createListOfActivityElements(R.array.abc));
        ArrayList<ActivityElement> allCategoriesList = new ArrayList<>(elementsLists.getLettersList());
        elementsLists.setRandomList(allCategoriesList);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getBoolean(STUDY_SWITCH, false)) {
            elementsLists.setGuessList(allCategoriesList);
        } else {
            elementsLists.loadGuessActivity(this);
        }
        return elementsLists;
    }

    private List<ActivityElement> createListOfActivityElements(int resourceArray) {
        List<ActivityElement> list = new ArrayList<>();
        TypedArray typedArray = getResources().obtainTypedArray(resourceArray);
        for (int i = 0; i < typedArray.length(); i++) {
            int resourceId = typedArray.getResourceId(i, 0);
            ActivityElement element = ActivityElement.getElementByTypedArray(getResources().obtainTypedArray(resourceId));
            list.add(element);
        }
        typedArray.recycle();
        return list;
    }

}