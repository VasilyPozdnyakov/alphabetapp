package com.avappinc.sounds;

import android.content.res.TypedArray;

public class ActivityElement {

    private static final int NAME_INDEX = 0;
    private static final int IMAGE1_INDEX = 1;
    private static final int IMAGE2_INDEX = 2;
    private static final int SOUND1_INDEX = 3;
    private static final int SOUND2_INDEX = 4;

    private String name;
    private int image1, image2;
    private int sound1, sound2;

    public ActivityElement(String name, int image1, int image2, int sound1, int sound2) {
        this.name = name;
        this.image1 = image1;
        this.image2 = image2;
        this.sound1 = sound1;
        this.sound2 = sound2;
    }

    public static ActivityElement getElementByTypedArray(TypedArray typedArray) {
        String name = typedArray.getString(NAME_INDEX);
        int image1 = typedArray.getResourceId(IMAGE1_INDEX, 0);
        int image2 = typedArray.getResourceId(IMAGE2_INDEX, 0);
        int sound1 = typedArray.getResourceId(SOUND1_INDEX, 0);
        int sound2 = typedArray.getResourceId(SOUND2_INDEX, 0);
        return new ActivityElement(name, image1, image2, sound1, sound2);
    }

    public String getName() {
        return name;
    }

    public int getImage1() {
        return image1;
    }

    public int getImage2() {
        return image2;
    }

    public int getSound1() {
        return sound1;
    }

    public int getSound2() {
        return sound2;
    }

}
