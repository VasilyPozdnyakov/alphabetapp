package com.avappinc.sounds;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;

import static com.avappinc.sounds.SettingsActivity.SOUND_SWITCH;

public class Music {

    public static final Music INSTANCE = new Music();
    public static MediaPlayer musicPool;
    public static MediaPlayer mediaPlayer;

    private Music() {
    }

    public void initMusic(Context context) {
        mediaPlayer = MediaPlayer.create(context, R.raw.music);
        mediaPlayer.setLooping(true);

        musicPool = new MediaPlayer();
    }

    public void pauseMainMusic() {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
        } catch (IllegalStateException ex) {
            System.out.println(ex);
        }

    }

    public void pauseMusic() {
        try {
            if (musicPool != null && musicPool.isPlaying()) {
                musicPool.pause();
            }
        } catch (IllegalStateException ex) {
            System.out.println(ex);
        }
    }


    public void playMusic(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.getBoolean(SOUND_SWITCH, true)) {
            mediaPlayer.start();
        }
    }

}
