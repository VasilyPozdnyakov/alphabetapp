package com.avappinc.sounds;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.avappinc.sounds.InitialActivity.activityElementsLists;
import static com.avappinc.sounds.Music.musicPool;
import static com.avappinc.sounds.SettingsActivity.STUDY_SWITCH;

public class CategoryActivity extends AppCompatActivity {

    private ArrayList<String> list = new ArrayList<>();
    private List<Integer> listInt = new ArrayList<>();
    private int currentIndex = 0;
    private int clickNumber = 0;
    private ImageView image, nextButton, prevButton, letter;
    private Order order;
    private boolean musicPlaying = false;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            order = (Order) extras.get(ChooseActivity.REGIME);
            list = extras.getStringArrayList(ChooseActivity.LIST);
        } else {
            order = Order.ABC;
        }

        image = findViewById(R.id.allView);
        letter = findViewById(R.id.letterView);
        nextButton = findViewById(R.id.nextView);
        prevButton = findViewById(R.id.prevView);

        Category category = Category.LETTER;
        ElementsLists elementsList = InitialActivity.activityElementsLists;

        for (String item : list) {
            listInt.add(Integer.parseInt(item.substring(10)));
        }

        if (order == Order.ABC) {
            Collections.sort(listInt);
        } else {
            if (order == Order.ONTO) {

                int[] ontoInt = getResources().getIntArray(R.array.ontoInt);
                List<Integer> sorted = new ArrayList<>(ontoInt.length);
                for (int i : ontoInt) {
                    sorted.add(i);
                }

                Collections.sort(listInt, Comparator.comparing(sorted::indexOf));
            }
        }

        //System.out.println(listInt);
        //Toast toast = Toast.makeText(getApplicationContext(), elementsList.getLettersList().get(listInt.get(0)-1).getName()+
        //        " ", Toast.LENGTH_SHORT);
        //toast.show();

        final List<ActivityElement> activityElementList = getActivityElementList(elementsList, category);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        letter.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(0) - 1).getImage1()));
        image.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(0) - 1).getImage2()));

        image.setOnClickListener(
                v -> {
                    if (musicPlaying) {
                        return;
                    }
                    clickNumber++;
                    musicPool.release();

                    if (clickNumber == 1) {
                        musicPool = MediaPlayer.create(this, activityElementList.get(listInt.get(currentIndex) - 1).getSound1());
                    } else {
                        clickNumber = 0;
                        musicPool = MediaPlayer.create(this, activityElementList.get(listInt.get(currentIndex) - 1).getSound2());
                    }

                    musicPool.setOnPreparedListener(mp -> {
                        musicPool.start();
                        musicPlaying = true;
                    });
                    musicPool.setOnCompletionListener(mp -> {
                        musicPool.release();
                        musicPlaying = false;
                    });
                    if (preferences.getBoolean(STUDY_SWITCH, false) && !elementsList.getGuessList()
                            .contains(activityElementList.get(listInt.get(currentIndex) - 1))) {
                        elementsList.getGuessList().add(activityElementList.get(listInt.get(currentIndex) - 1));
                        elementsList.saveGuessList(this);
                    }
                }
        );

        nextButton.setOnClickListener(
                v -> {
                    currentIndex = currentIndex + 1;
                    if (currentIndex > listInt.size() - 1) {
                        currentIndex = 0;
                    }
                    image.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(currentIndex) - 1).getImage2()));
                    letter.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(currentIndex) - 1).getImage1()));
                    Music.INSTANCE.pauseMusic();
                    clickNumber = 0;
                }
        );
        prevButton.setOnClickListener(
                v -> {
                    currentIndex = currentIndex - 1;
                    if (currentIndex < 0) {
                        currentIndex = listInt.size() - 1;
                    }
                    image.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(currentIndex) - 1).getImage2()));
                    letter.setImageDrawable(getResources().getDrawable(activityElementList.get(listInt.get(currentIndex) - 1).getImage1()));
                    Music.INSTANCE.pauseMusic();
                    clickNumber = 0;
                }
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        Music.INSTANCE.pauseMusic();
    }

    @Override
    public void finish() {
        super.finish();
        activityElementsLists.saveGuessList(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMusic();
    }

    private List<ActivityElement> getActivityElementList(ElementsLists elementsLists, Category category) {
        List<ActivityElement> list;
        if (category == Category.LETTER) {
            list = elementsLists.getLettersList();
        } else {
            list = elementsLists.getRandomList();
        }
        return list;
    }

}
